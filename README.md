<img src="./src/assets/screen1.png" alt="project" />
<br />

# ✅ vue-tracker 🗺️📍

## 📐 Project setup
```
npm install
```

### 🏍️ Compiles and hot-reloads for development
```
npm run serve
```

### 🧱 Compiles and minifies for production
```
npm run build
```
